﻿//-----------------------------------------------------------------------
// <copyright file="SQLConfig.cs" company="Rushkar">
//     Copyright Rushkar. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace YEP.Data
{
    /// <summary>
    /// SQL configuration class which holds stored procedure name.
    /// </summary>
    internal sealed class SQLConfig
    {
        //#region LookUpServices
        //public const string LookUpServices_All = "LookUpServices_All";
        //public const string LookUpServices_ParentId = "LookUpServices_ParentId";
        //public const string LookUpServices_ById = "LookUpServices_ById";
        //public const string LookUpServices_Upsert = "LookUpServices_Upsert";
        //public const string LookUpServices_ActInact = "LookUpServices_ActInact";
        //public const string LookUpServices_Delete = "LookUpServices_Delete";
        //#endregion
        
    }
}
