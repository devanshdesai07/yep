﻿using System.Collections.Generic;

namespace YEP.Common.Paging
{
    /// <summary>
    /// Paged list interface
    /// </summary>
    public interface IPagedList<T> : IList<T>
    {
        
    }
}
