﻿using System.Collections.Generic;

namespace YEPAdmin.Pages
{
    public class Controllers
    {
        public const string Authentication = "Authentication";
        public const string Home = "Home";
        public const string Users = "Users";
        public const string SalonOwner = "SalonOwner";
        public const string SalonsData = "SalonsData";
        public const string ChangePassword = "ChangePassword";
        public const string MasterData = "MasterData";
    }
}