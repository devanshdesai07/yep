﻿namespace YEPAdmin.Pages
{
    public class Actions
    {
        public const string Signin = "Signin";
        public const string Salons = "Salons";
        public const string Signout = "Signout";
        public const string Index = "Index";
        public const string Delete = "Delete";
        public const string ViewAllData = "ViewAllData";
        public const string ViewAllDataSalons = "ViewAllDataSalons";
        public const string ViewAllDataCustomer = "ViewAllDataCustomer";
        public const string ActiveInActive = "ActiveInActive";
        public const string UserGallery = "UserGallery";
        public const string ChangeStatusSalon = "ChangeStatusSalon";
        public const string ManageCustomers = "ManageCustomers";
        public const string ManageEmployeeLeaves = "ManageEmployeeLeaves";
        public const string ViewAllDataEmployees = "ViewAllDataEmployees";
        public const string ViewAllDataEmployeeLeaves = "ViewAllDataEmployeeLeaves";
        public const string ManageEmployeeWorksheet = "ManageEmployeeWorksheet";
        public const string ViewAllDataEmployeeWorkSheet = "ViewAllDataEmployeeWorkSheet";
        public const string ManageEmployees = "ManageEmployees";
        public const string ManageSalonServices = "ManageSalonServices";
        public const string ViewAllDataSalonServices = "ViewAllDataSalonServices";
        public const string ChangePassword = "ChangePassword";
        public const string ViewAllDataAppointments = "ViewAllDataAppointments";
        public const string ManageAppointments = "ManageAppointments";
        public const string BindServicesDropdown = "BindServicesDropdown";
        public const string ViewDataCustomerDetails = "ViewDataCustomerDetails";
        public const string ViewAllDataEmployeeLeavecount = "ViewAllDataEmployeeLeavecount";
        public const string GetAppointmentDetaislById = "GetAppointmentDetaislById";
        public const string ManageEmployeeAndCharges = "ManageEmployeeAndCharges";
        public const string ViewAllDataEmployeeAndCharges = "ViewAllDataEmployeeAndCharges";
        public const string ViewAllDataMasterProductType = "ViewAllDataMasterProductType";
        public const string MasterProductType = "MasterProductType";
        public const string ActiveInActiveProductType = "ActiveInActiveProductType";
        public const string DeleteProductType = "DeleteProductType";
        public const string ProductTypeUpsert = "ProductTypeUpsert";
        public const string GetDataProductTypeById = "GetDataProductTypeById";

        public const string GetDataProductBrandById = "GetDataProductBrandById";
        public const string ProductBrandUpsert = "ProductBrandUpsert";
        public const string DeleteProductBrand = "DeleteProductBrand";
        public const string ViewAllDataMasterProductBrand = "ViewAllDataMasterProductBrand";
        public const string ActiveInActiveProductBrand = "ActiveInActiveProductBrand";
        public const string MasterProductBrand = "MasterProductBrand";

        public const string ViewAllDataMasterProductWeight = "ViewAllDataMasterProductWeight";
        public const string ActiveInActiveProductWeight = "ActiveInActiveProductWeight";
        public const string DeleteProductWeight = "DeleteProductWeight";
        public const string ProductWeightUpsert = "ProductWeightUpsert";
        public const string GetDataProductweightById = "GetDataProductweightById";
        public const string MasterProductWeight = "MasterProductWeight";

        public const string ViewAllDataMasterOrderStatus = "ViewAllDataMasterOrderStatus";
        public const string ActiveInActiveOrderStatus = "ActiveInActiveOrderStatus";
        public const string DeleteOrderStatus = "DeleteOrderStatus";
        public const string OrderStatusUpsert = "OrderStatusUpsert";
        public const string GetDataOrderstatusById = "GetDataOrderstatusById";
        public const string MasterOrderStatus = "MasterOrderStatus";
        public const string MasterProductCategory = "MasterProductCategory";
        public const string ViewAllDataMasterProductCategory = "ViewAllDataMasterProductCategory";
        public const string ActiveInActiveProductCategory = "ActiveInActiveProductCategory";
        public const string DeleteProductCategory = "DeleteProductCategory";
        public const string ProductCategoryUpsert = "ProductCategoryUpsert";
        public const string GetDataProductCategoryById = "GetDataProductCategoryById";
    }
}